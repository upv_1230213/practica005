package upv.com.practica_5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends Activity {
    public final String mensaje = "com.upv.practica_5.mensaje";
    Button manzanab, uvab, pinab, sandiab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manzanab = (Button)findViewById(R.id.manzanab);
        uvab = (Button)findViewById(R.id.uvab);
        pinab = (Button)findViewById(R.id.pinab);
        sandiab = (Button)findViewById(R.id.sandiab);
    }

    public void verManzana(View v){
        Intent intent = new Intent(this, manzana.class);
        intent.putExtra(mensaje, "manzana1.png");
        startActivity(intent);
    }
    public void verUva(View v){
        Intent intent = new Intent(this, uva.class);
        intent.putExtra(mensaje, "uva1.png");
        startActivity(intent);
    }
    public void verPina(View v){
        Intent intent = new Intent(this, pina.class);
        intent.putExtra(mensaje, "pina1.png");
        startActivity(intent);
    }
    public void verSandia(View v){
        Intent intent = new Intent(this, sandia.class);
        intent.putExtra(mensaje, "sandia1.png");
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
